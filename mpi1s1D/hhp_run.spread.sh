. hhp_bashrc.sh
#cd ../run/180x120
#mpirun -np 96 ../../mpi2s1D/cgpop.linux.180x120 < ./pop_in
#cd ../../mpi2s1D



mpirun --report-bindings --bind-to core -np 96 ./cgpop.linux.180x120 < ../run/180x120/pop_in 2> stderr.spread.96.log | tee stdout.spread.96.log
mpirun --report-bindings --bind-to core -np 88 ./cgpop.linux.180x120 < ../run/180x120/pop_in 2> stderr.spread.88.log | tee stdout.spread.88.log
mpirun --report-bindings --bind-to core -np 80 ./cgpop.linux.180x120 < ../run/180x120/pop_in 2> stderr.spread.80.log | tee stdout.spread.80.log
mpirun --report-bindings --bind-to core -np 72 ./cgpop.linux.180x120 < ../run/180x120/pop_in 2> stderr.spread.72.log | tee stdout.spread.72.log
mpirun --report-bindings --bind-to core -np 64 ./cgpop.linux.180x120 < ../run/180x120/pop_in 2> stderr.spread.64.log | tee stdout.spread.64.log
mpirun --report-bindings --bind-to core -np 56 ./cgpop.linux.180x120 < ../run/180x120/pop_in 2> stderr.spread.56.log | tee stdout.spread.56.log
mpirun --report-bindings --bind-to core -np 48 ./cgpop.linux.180x120 < ../run/180x120/pop_in 2> stderr.spread.48.log | tee stdout.spread.48.log
mpirun --report-bindings --bind-to core -np 40 ./cgpop.linux.180x120 < ../run/180x120/pop_in 2> stderr.spread.40.log | tee stdout.spread.40.log
mpirun --report-bindings --bind-to core -np 32 ./cgpop.linux.180x120 < ../run/180x120/pop_in 2> stderr.spread.32.log | tee stdout.spread.32.log
mpirun --report-bindings --bind-to core -np 24 ./cgpop.linux.180x120 < ../run/180x120/pop_in 2> stderr.spread.24.log | tee stdout.spread.24.log


#mpirun -np 96 ./cgpop.linux.180x120 < ../run/180x120/pop_in | tee mm.96.log
#mpirun -np 48 ./cgpop.linux.180x120 < ../run/180x120/pop_in | tee mm.48.log
#mpirun -np 24 ./cgpop.linux.180x120 < ../run/180x120/pop_in | tee mm.24.log
#mpirun -np 40 ./cgpop.linux.180x120 < ../run/180x120/pop_in
#mpirun -np 40 ./cgpop.linux.180x120 < ../run/180x120/pop_in
#mpirun -np 40 ./cgpop.linux.180x120 < ../run/180x120/pop_in

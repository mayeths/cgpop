. hhp_bashrc.sh
#cd ../run/180x120
#mpirun -np 96 ../../mpi2s1D/cgpop.linux.180x120 < ./pop_in
#cd ../../mpi2s1D

mpirun --report-bindings --bind-to cpu-list:ordered --cpu-list 0-95 -np 96 ./cgpop.linux.180x120 < ../run/180x120/pop_in | tee mm.ordered.96.log
mpirun --report-bindings --bind-to cpu-list:ordered --cpu-list 0-95 -np 88 ./cgpop.linux.180x120 < ../run/180x120/pop_in | tee mm.ordered.88.log
mpirun --report-bindings --bind-to cpu-list:ordered --cpu-list 0-95 -np 80 ./cgpop.linux.180x120 < ../run/180x120/pop_in | tee mm.ordered.80.log
mpirun --report-bindings --bind-to cpu-list:ordered --cpu-list 0-95 -np 72 ./cgpop.linux.180x120 < ../run/180x120/pop_in | tee mm.ordered.72.log
mpirun --report-bindings --bind-to cpu-list:ordered --cpu-list 0-95 -np 64 ./cgpop.linux.180x120 < ../run/180x120/pop_in | tee mm.ordered.64.log
mpirun --report-bindings --bind-to cpu-list:ordered --cpu-list 0-95 -np 56 ./cgpop.linux.180x120 < ../run/180x120/pop_in | tee mm.ordered.56.log
mpirun --report-bindings --bind-to cpu-list:ordered --cpu-list 0-95 -np 48 ./cgpop.linux.180x120 < ../run/180x120/pop_in | tee mm.ordered.48.log
mpirun --report-bindings --bind-to cpu-list:ordered --cpu-list 0-95 -np 40 ./cgpop.linux.180x120 < ../run/180x120/pop_in | tee mm.ordered.40.log
mpirun --report-bindings --bind-to cpu-list:ordered --cpu-list 0-95 -np 32 ./cgpop.linux.180x120 < ../run/180x120/pop_in | tee mm.ordered.32.log
mpirun --report-bindings --bind-to cpu-list:ordered --cpu-list 0-95 -np 24 ./cgpop.linux.180x120 < ../run/180x120/pop_in | tee mm.ordered.24.log


#mpirun -np 96 ./cgpop.linux.180x120 < ../run/180x120/pop_in | tee mm.96.log
#mpirun -np 48 ./cgpop.linux.180x120 < ../run/180x120/pop_in | tee mm.48.log
#mpirun -np 24 ./cgpop.linux.180x120 < ../run/180x120/pop_in | tee mm.24.log
#mpirun -np 40 ./cgpop.linux.180x120 < ../run/180x120/pop_in
#mpirun -np 40 ./cgpop.linux.180x120 < ../run/180x120/pop_in
#mpirun -np 40 ./cgpop.linux.180x120 < ../run/180x120/pop_in

#!/bin/bash

if uname -m | grep "x86" >/dev/null; then
    echo "You are using x86 machine. (Huang Haopeng)"
    exit 1
fi

MAX=1
#CORES=(96 92 88 84 80 76 72 68 64 60 56 52 48 44 40 36 32 28 24)
CORES=(96 88 80 72 64 56 48 40 32 24)
#CORES=(40 48 64 72 96)

HOST=node102
WORK_ROOT="/storage/hhp_share/project/cgpop/mpi2s1D"
EXE="$WORK_ROOT/cgpop.linux.180x120"

NX=100
NY=100
NZ=100
SUFFIX=$(date +%y%m%d-%H%M%S)
OPT="--cpu-set 0-127"
OPT="$OPT --bind-to cpulist:ordered --report-bindings"
OPT="$OPT -host $HOST:128 -x LD_LIBRARY_PATH"

source hhp_bashrc.sh
echo "Running $EXE on $HOST:$WORK_ROOT"

doit() {
    if [[ $# -ne 2 ]]; then
        echo "Wrong argument number $#"
        return 1
    fi
    local CORE=$1
    local ID=$2

    echo "mpirun $OPT -np $CORE $EXE < $WORK_ROOT/../run/180x120/pop_in"
    #mpirun $OPT -np $CORE $EXE < $WORK_ROOT/../run/180x120/pop_in > stdout 2> stderr
    mpirun $OPT -np $CORE $EXE < $WORK_ROOT/../run/180x120/pop_in | tee ./mm.$CORE.stdout 2> ./mm.$CORE.stderr

    #local LOG_NAME="cgpoplog.$CORE.$ID.$SUFFIX"
    #local LOG="$WORK_ROOT/$LOG_NAME"
    #mkdir -p $LOG
    #mv stdout stderr pcg_* timing.0000* $LOG
}

for i in $(seq 1 $MAX); do

    for CORE in ${CORES[@]}; do
        echo ">>> ($i/$MAX) Working on $CORE"
        doit $CORE $i
    done

done


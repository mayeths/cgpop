#!/bin/bash

echo ">>>>> 1s1D"
cd mpi1s1D
./hhp_run.sh
cd ..

echo ">>>>> 2s1D"
cd mpi2s1D
./hhp_run.sh
cd ..

echo ">>>>> 2s1D_overlap"
cd mpi2s1D_overlap
./hhp_run.sh
cd ..

echo ">>>>> 2s2D"
cd mpi2s2D
./hhp_run.sh
cd ..


ARR=(120x80  180x120  18x12  24x16  36x24  48x32  60x40  90x60)
#CORE=(4 8 12 16 24 32 36 48 64 72 96)
CORE=$(seq 4 4 96)

for A in ${ARR[@]}; do
    cd $A
    printf "$A "
    for C in ${CORE[@]}; do
        ZZ=$(mpirun -np $C ../../mpi2s1D/cgpop.linux.$A < ./pop_in 2>&1)
        ERR=$(echo $ZZ | grep "PMIX ERROR")
        if [[ $? -ne 0 ]] ; then
            printf "$C "
        fi
    done
    printf "\n"
    cd ..
done

